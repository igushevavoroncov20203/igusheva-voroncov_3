#include "Lily.h"
#include <iostream>
#include <windows.h>
#include <time.h>
#include <conio.h>
#include <cstdlib>
#include <cmath>
#include <iomanip>

using namespace std;

extern int Matrix[14][14];

void SumOfElementsOfHalfTheMatrix()
{
    int Sum = 0;
    for (int i = 0; i < (14 / 2); i++)
        for (int j = 0; j < (14 / 2); j++)
        {
            Sum += Matrix[i][j];
        }
    cout << "Summa elements = " << Sum << endl;
}