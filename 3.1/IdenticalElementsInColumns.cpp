#include "Lily.h"
#include <iostream>
#include <windows.h>
#include <time.h>
#include <conio.h>
#include <cstdlib>
#include <cmath>
#include <iomanip>

using namespace std;

extern int Matrix[14][14];

void IdenticalElementsInColumns()
{
    bool flag = 0;
    for (int i = 0; i < 14; i++)
    {
        for (int j = 0; j < 14; j++)
        {
            if (Matrix[i][0] == Matrix[j][13])
            {
                flag = 1;
            }
        }
    }
    if (flag)
        cout << "Yes!!\n";
    else
        cout << "No:(\n";
}