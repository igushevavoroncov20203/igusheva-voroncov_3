#include "Lily.h"
#include <iostream>
#include <windows.h>
#include <time.h>
#include <conio.h>
#include <cstdlib>
#include <cmath>
#include <iomanip>

using namespace std;

static int Matrix[14][14];

void OutputMatrix()
{
    for (int i = 0; i < 14; i++)
    {
        for (int j = 0; j < 14; j++)
        {
            cout << setw(4) << right << Matrix[i][j];
        }
        cout << endl;
    }
}