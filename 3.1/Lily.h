#pragma once

int getRandomNumber(int min, int max);
void EnterMatrix();
void OutputMatrix();
void ProductAfterMinModulo();
void IdenticalElementsInColumns();
void MaxElementOfHalfTheMatrix();
void SumOfElementsOfHalfTheMatrix();