#include "Lily.h"
#include <iostream>
#include <windows.h>
#include <time.h>
#include <conio.h>
#include <cstdlib>
#include <cmath>
#include <iomanip>

using namespace std;

extern int Matrix[14][14];

void MaxElementOfHalfTheMatrix()
{
    int Max = Matrix[1][1];
    for (int i = 0; i < (14 / 2); i++)
        for (int j = 0; j < (14 / 2); j++)
        {
            if (Matrix[i][j] > Max)
                Max = Matrix[i][j];
        }
    cout << "Max element = " << Max << endl;
}