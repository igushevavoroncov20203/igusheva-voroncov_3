#include "Lily.h"
#include <iostream>
#include <windows.h>
#include <time.h>
#include <conio.h>
#include <cstdlib>
#include <cmath>
#include <iomanip>

using namespace std;

extern int Matrix[14][14];

void ProductAfterMinModulo()
{
    for (int j = 0; j < 14; j++)
    {
        int Comp = 1, ItemNumber = 1, Min = Matrix[1][j];
        for (int i = 0; i < 14; i++)
        {
            if (abs(Matrix[i][j]) <= abs(Min))
            {
                Min = Matrix[i][j];
                ItemNumber = i;
            }
        }
        if (ItemNumber < 13)
        {
            for (int k = (ItemNumber + 1); k < 14; k++)
                Comp *= Matrix[k][j];
        }
        else
            Comp = 0;
        cout << setw(6) << right << Comp << "\n";
    }
}