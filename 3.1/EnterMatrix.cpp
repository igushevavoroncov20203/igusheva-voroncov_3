#include "Lily.h"
#include <iostream>
#include <windows.h>
#include <time.h>
#include <conio.h>
#include <cstdlib>
#include <cmath>
#include <iomanip>

using namespace std;

extern int Matrix[14][14];

int getRandomNumber(int min, int max)
{
    static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);
    return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}

void EnterMatrix()
{
    for (int i = 0; i < 14; i++)
        for (int j = 0; j < 14; j++)
        {
            ::Matrix[i][j] = getRandomNumber(-6, 6);
        }
}