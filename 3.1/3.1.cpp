#include <iostream>
#include <string>
#include <conio.h>
#include <windows.h>
#include "Lily.h"
#include <iomanip>

using namespace std;

 int Matrix[14][14];
 string Menu[6];
 int x, y;
 int paragraph;

 void GoToXY(int xpos, int ypos)
 {
     COORD scrn;
     HANDLE hOuput = GetStdHandle(STD_OUTPUT_HANDLE);
     scrn.X = xpos; scrn.Y = ypos;
     SetConsoleCursorPosition(hOuput, scrn);
 }

void SetColor(int text, int background)
{
    HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdOut, (WORD)((background << 4) | text));
}

void MenuToScreen()
{
    system("CLS");
    for (int i = 0; i <= 5; i++)
    {
        GoToXY(::x, ::y + i);
        cout << ::Menu[i];
    }
    SetColor(2,15);
    GoToXY (::x, ::y + ::paragraph);
    cout << ::Menu[::paragraph];
    SetColor(0, 15);
}

void main()
{
    paragraph = 0;
    int flag;
    Menu[0] = "Enter matrix";
    Menu[1] = "Show matrix";
    Menu[2] = "Task A";
    Menu[3] = "Task B";
    Menu[4] = "Task C";
    Menu[5] = "Exit";
    x = y = 5;
    SetColor(0, 15);
    MenuToScreen();
    do
    {
        flag = _getch();
        if (flag == 224)
        {
            flag = _getch();
            switch (flag)
            {
            case 80: 
            {
                if (paragraph < 5)
                {
                    GoToXY(x, y + paragraph);
                    cout << Menu[paragraph];
                    paragraph++;
                    SetColor(2, 15);
                    GoToXY(x, y + paragraph);
                    cout << Menu[paragraph];
                    SetColor(0, 15);
                }
                break;
            }
            case 72: 
            {
                if (paragraph > 0)
                {
                    GoToXY(x, y + paragraph);
                    cout << Menu[paragraph];
                    paragraph--;
                    SetColor(2, 15);
                    GoToXY(x, y + paragraph);
                    cout << Menu[paragraph];
                    SetColor(0, 15);
                }
                break;
            }
            }
        }
        else
        {
            if (flag == 13)
            {
                switch (paragraph)
                {
                case 0: {system("CLS"); EnterMatrix(); cout << "Matrix completed successfully\n"; system("pause"); break; }
                case 1: {system("CLS"); OutputMatrix(); cout << endl; system("pause"); break; }
                case 2: {system("CLS"); ProductAfterMinModulo(); system("pause"); break; }
                case 3: {system("CLS"); IdenticalElementsInColumns(); system("pause"); break; }
                case 4: {system("CLS"); MaxElementOfHalfTheMatrix(); SumOfElementsOfHalfTheMatrix(); system("pause"); break; }
                case 5: {system("CLS"); flag = 27; break; }
                }
            }
            MenuToScreen();
        }
    } while (flag != 27);
}